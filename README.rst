.. contents:: Table of Contents

========
Overview
========

The SPECS Core Enforcement Implementation component orchestrates acquisition of resources and deployment and configuration of security mechanisms responsible for enforcing and monitoring security features agreed in the SLA. It is responsible for:

- executing implementation plan and acquiring resources
- executing remediation plan to reconfigure target services during SLA remediation
- executing reaction plan to reconfigure target services after SLA renegotiation or SLA termination

The Implementation component is integrated with the Broker mechanism and a Chef Server.

Executing implementation plan:

.. image:: https://bitbucket.org/specs-team/specs-core-enforcement-implementation/raw/master/docs/images/implementation_after_planning.png

Implementing remediation plan:

.. image:: https://bitbucket.org/specs-team/specs-core-enforcement-implementation/raw/master/docs/images/implementation_after_remediation_and_renegotiation_and_SLA_termination.png

More details about the component can be found in the deliverable 4.2.2 available  at `http://www.specs-project.eu/publications/public-deliverables/d4-2-2 <http://www.specs-project.eu/publications/public-deliverables/d4-2-2>`_.

============
Installation
============

Prerequisites:

- Java web container
- MongoDB
- Java 7
- Chef Server

The Implementation component is packaged as a web application archive (WAR) file with the name implementation-api.war. The archive file can be built from source code or downloaded from the SPECS maven repository::

 https://nexus.services.ieat.ro/nexus/content/repositories/specs-snapshots

To build the Implementation component from source code you need the Apache Maven 3 tool. First clone the project from the Bitbucket repository using a Git client::

 git clone git@bitbucket.org:specs-team/specs-core-enforcement-implementation.git

then go into the specs-core-enforcement-implementation directory and run::

 mvn package

The implementation-api.war file is located in the implementation-api/target directory.

The implementation-api.war file has to be deployed to a Java web container. For example, to deploy the application to Apache Tomcat 7, just copy the war file to the Tomcat webapps directory::

 cp implementation-api/target/implementation-api.war /var/lib/tomcat7/webapps/

The application configuration is located in the file *implementation.properties* in the Java properties format. The file contains the following configuration properties:

.. code-block:: jproperties

 sla-manager-api.address=https://localhost/sla-manager-api/sla_manager_rest_api
 planning-api.address=https://localhost/planning-api
 event-archiver.address=https://localhost/event-archiver
 auditing-api.address=https://localhost/auditing
 monitoring-api.address=https://localhost/monitoring
 mongodb.host=localhost
 mongodb.port=27017
 mongodb.database=enforcement-implementation
 chef-server.organization=mos
 chef-server.organizationPK=
 chef-server.endpoint=
 chef-server.username=
 chef-server.password=

Make the necessary changes and restart the web container for changes to take effect. The Implementation API should now be available at::

 https://<host>:<port>/implementation-api

======
Notice
======

This product includes software developed at "XLAB d.o.o, Slovenia", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).

- http://www.specs-project.eu/
- http://www.xlab.si/

Developers:

- Jolanda Modic, jolanda.modic@xlab.si
- Damjan Murn, damjan.murn@xlab.si

Copyright:

.. code-block:: 

 Copyright 2013-2015, XLAB d.o.o, Slovenia
    http://www.xlab.si

 SPECS Core Enforcement Implementation is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License, version 3,
 as published by the Free Software Foundation.

 SPECS Core Enforcement Implementation is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.