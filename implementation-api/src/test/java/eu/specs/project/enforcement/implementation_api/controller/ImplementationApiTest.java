package eu.specs.project.enforcement.implementation_api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;
import eu.specs.project.enforcement.implementation.core.util.JsonDumper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:implementation-api-context-test.xml")
public class ImplementationApiTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(ImplementationApiTest.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Provisioner provisioner;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void test() throws Exception {

        ImplementationPlan implPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/impl-plan.json"), ImplementationPlan.class);

        // define expectations for mocks
        ImplementationPlan implPlan1 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/impl-plan.json"), ImplementationPlan.class);
        implPlan1.setId(UUID.randomUUID().toString());
        when(provisioner.deployVms(any(ImplementationPlan.class))).thenReturn(implPlan1);

        // create implementation activity
        MvcResult result = mockMvc.perform(post("/impl-activities")
                .content(objectMapper.writeValueAsString(implPlan)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        ImplActivity implActivity = objectMapper.readValue(
                result.getResponse().getContentAsString(), ImplActivity.class);
        assertEquals(implActivity.getImplPlanId(), implPlan.getId());

        // wait until the implementation activity finishes
        Date startTime = new Date();
        do {
            logger.trace("Waiting for the implementation activity to finish...");
            Thread.sleep(200);
            result = mockMvc.perform(get("/impl-activities/{0}/status", implActivity.getId()))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.TEXT_PLAIN))
                    .andReturn();
            ImplActivity.Status status = ImplActivity.Status.valueOf(result.getResponse().getContentAsString());

            if (status == ImplActivity.Status.ACTIVE) {
                break;
            } else if (status == ImplActivity.Status.ERROR) {
                fail("Implementation activity failed.");
            }
            if (new Date().getTime() - startTime.getTime() > 10000) {
                fail("Timeout waiting for the implementation activity to finish.");
            }
        } while (true);

        // retrieve implementation activity
        result = mockMvc.perform(get("/impl-activities/{0}", implActivity.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        ImplActivity implActivity1 = objectMapper.readValue(
                result.getResponse().getContentAsString(), ImplActivity.class);

        logger.trace("Impl activity:\n{}", JsonDumper.dump(implActivity1));
    }
}