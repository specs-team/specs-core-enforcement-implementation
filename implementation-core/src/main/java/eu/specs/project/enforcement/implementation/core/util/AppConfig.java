package eu.specs.project.enforcement.implementation.core.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

    @Value("${auditing-api.address}")
    private String auditingApiAddress;

    @Value("${event-archiver.address}")
    private String eventArchiverAddress;

    @Value("${remediation-event.timeout}")
    private int remediationEventTimeout;

    @Value("${remediation-event.query-interval}")
    private int remediationEventQueryInterval;

    public String getAuditingApiAddress() {
        return auditingApiAddress;
    }

    public String getEventArchiverAddress() {
        return eventArchiverAddress;
    }

    public int getRemediationEventTimeout() {
        return remediationEventTimeout;
    }

    public int getRemediationEventQueryInterval() {
        return remediationEventQueryInterval;
    }
}
