package eu.specs.project.enforcement.implementation.core.repository;

import eu.specs.datamodel.enforcement.RemPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class RemPlanRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public RemPlan findById(String remPlanId) {
        return mongoTemplate.findById(remPlanId, RemPlan.class);
    }

    public void save(RemPlan remPlan) {
        mongoTemplate.save(remPlan);
    }

    public void delete(RemPlan remPlan) {
        mongoTemplate.remove(remPlan);
    }
}
