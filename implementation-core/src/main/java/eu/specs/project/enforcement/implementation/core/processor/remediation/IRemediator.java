package eu.specs.project.enforcement.implementation.core.processor.remediation;

import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.implementation.core.exception.ImplementationException;

public interface IRemediator {

    boolean isApplicable(RemPlan remPlan);

    RemPlan.Result remediate(RemPlan remPlan, ImplementationPlan implPlan) throws ImplementationException;

}
