package eu.specs.project.enforcement.implementation.core.exception;

public class ImplementationException extends Exception {

    public ImplementationException() {
        super();
    }

    public ImplementationException(String message) {
        super(message);
    }

    public ImplementationException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
