package eu.specs.project.enforcement.implementation.core.repository;

import eu.specs.datamodel.enforcement.ImplementationPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ImplPlanRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public List<ImplementationPlan> getAll(String slaId) {
        Query query = new Query();
        if (slaId != null) {
            query.addCriteria(Criteria.where("slaId").is(slaId));
        }

        query.fields().include("id");
        return mongoTemplate.find(query, ImplementationPlan.class);
    }

    public ImplementationPlan findById(String id) {
        return mongoTemplate.findById(id, ImplementationPlan.class);
    }

    public void save(ImplementationPlan implPlan) {
        mongoTemplate.save(implPlan);
    }

    public void delete(ImplementationPlan implPlan) {
        mongoTemplate.remove(implPlan);
    }
}
