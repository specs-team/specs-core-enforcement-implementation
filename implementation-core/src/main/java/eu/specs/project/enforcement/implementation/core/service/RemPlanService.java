package eu.specs.project.enforcement.implementation.core.service;

import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.implementation.core.processor.RemPlanProcessor;
import eu.specs.project.enforcement.implementation.core.repository.RemPlanRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class RemPlanService {
    private static final Logger logger = LogManager.getLogger(RemPlanService.class);
    public static final int EXECUTOR_NUMBER_OF_THREADS = 1;
    private ExecutorService executorService;

    @Autowired
    private RemPlanRepository remPlanRepository;

    @Autowired
    private RemPlanProcessor remPlanProcessor;

    public RemPlanService() {
        executorService = Executors.newFixedThreadPool(EXECUTOR_NUMBER_OF_THREADS);
    }

    public String implementRemPlan(final RemPlan remPlan) {
        logger.debug("implementRemPlan() started.");
        remPlan.setId(UUID.randomUUID().toString());
        remPlanRepository.save(remPlan);
        logger.debug("RemPlan has been stored with ID {}.", remPlan.getId());

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                remPlanProcessor.implementRemPlan(remPlan);
            }
        });

        logger.debug("implementRemPlan() finished successfully.");
        return remPlan.getId();
    }

    public RemPlan getRemPlan(String remPlanId) {
        return remPlanRepository.findById(remPlanId);
    }
}
