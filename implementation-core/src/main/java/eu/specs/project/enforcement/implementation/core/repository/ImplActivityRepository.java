package eu.specs.project.enforcement.implementation.core.repository;

import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.project.enforcement.implementation.core.service.ImplActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ImplActivityRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public ImplActivity findById(String id) {
        return mongoTemplate.findById(id, ImplActivity.class);
    }

    public List<ImplActivity> findAll(ImplActivityService.Filter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getStatus() != null) {
            query.addCriteria(Criteria.where("state").is(filter.getStatus().name()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, ImplActivity.class);
    }

    public void save(ImplActivity implActivity) {
        mongoTemplate.save(implActivity);
    }

    public void delete(ImplActivity implActivity) {
        mongoTemplate.remove(implActivity);
    }
}
