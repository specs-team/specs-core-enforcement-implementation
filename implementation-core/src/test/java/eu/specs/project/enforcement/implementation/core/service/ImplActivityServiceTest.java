package eu.specs.project.enforcement.implementation.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;
import eu.specs.project.enforcement.implementation.core.TestParent;
import eu.specs.project.enforcement.implementation.core.util.JsonDumper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:implementation-core-context-test.xml")
public class ImplActivityServiceTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(ImplActivityServiceTest.class);

    @Autowired
    private ImplActivityService implActivityService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Provisioner provisioner;

    @Test
    public void testImplementPlan() throws Exception {

        ImplementationPlan implPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/impl-plan.json"), ImplementationPlan.class);

        // define expectations for mocks
        ImplementationPlan implPlan1 = objectMapper.readValue(
                this.getClass().getResourceAsStream("/impl-plan.json"), ImplementationPlan.class);
        implPlan1.setId(UUID.randomUUID().toString());
        when(provisioner.deployVms(implPlan)).thenReturn(implPlan1);

        ImplActivity implActivity = implActivityService.implementPlan(implPlan);
        assertEquals(implActivity.getImplPlanId(), implPlan.getId());
        assertEquals(implActivity.getSlaId(), implPlan.getSlaId());

        Date startTime = new Date();
        while (implActivity.getState() == ImplActivity.Status.CREATED ||
                implActivity.getState() == ImplActivity.Status.IMPLEMENTING) {
            Thread.sleep(100);
            if (new Date().getTime() - startTime.getTime() > 10000) {
                fail("Timeout waiting for the implementation activity to finish.");
            }
        }
        logger.debug("Implementation activity finished:\n{}", JsonDumper.dump(implActivity));
        assertEquals(implActivity.getState(), ImplActivity.Status.ACTIVE);

        implActivityService.deleteImplActivity(implActivity);
        assertNull(implActivityService.getImplActivity(implActivity.getId()));
    }
}
