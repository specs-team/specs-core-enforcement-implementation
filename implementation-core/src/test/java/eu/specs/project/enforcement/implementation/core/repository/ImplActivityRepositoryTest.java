package eu.specs.project.enforcement.implementation.core.repository;

import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.project.enforcement.implementation.core.TestParent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:implementation-core-context-test.xml")
public class ImplActivityRepositoryTest extends TestParent {

    @Autowired
    private ImplActivityRepository scaRepository;

    @Test
    public void test() throws Exception {
        ImplActivity implActivity = new ImplActivity();
        implActivity.setId(UUID.randomUUID().toString());
        implActivity.setSlaId("SLA_ID");
        scaRepository.save(implActivity);

        ImplActivity implActivity1 = scaRepository.findById(implActivity.getId());
        assertEquals(implActivity1.getId(), implActivity1.getId());

        scaRepository.delete(implActivity1);
        assertNull(scaRepository.findById(implActivity1.getId()));
    }
}