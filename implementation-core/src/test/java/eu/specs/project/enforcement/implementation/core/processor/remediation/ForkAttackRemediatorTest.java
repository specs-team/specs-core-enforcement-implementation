package eu.specs.project.enforcement.implementation.core.processor.remediation;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;
import eu.specs.project.enforcement.implementation.core.TestParent;
import eu.specs.project.enforcement.implementation.core.service.ImplPlanService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:implementation-core-context-test.xml")
public class ForkAttackRemediatorTest extends TestParent {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ForkAttackRemediator forkAttackRemediator;

    @Autowired
    private Provisioner provisioner;

    @Autowired
    private ImplPlanService implPlanService;

    @Test
    public void testRemediate() throws Exception {

        RemPlan remPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/remediation/rem-plan-fork-attack.json"), RemPlan.class);

        final ImplementationPlan implPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/remediation/impl-plan-e2ee.json"), ImplementationPlan.class);

        // define expectations for mocks
        when(provisioner.addNewVmsToExistingGroup(implPlan)).thenAnswer(new Answer<ImplementationPlan>() {
            @Override
            public ImplementationPlan answer(InvocationOnMock invocationOnMock) throws Throwable {
                ImplementationPlan implPlanUpdated = objectMapper.readValue(
                        objectMapper.writeValueAsString(implPlan),
                        ImplementationPlan.class
                );
                ImplementationPlan.Vm slaveDbVm = implPlanUpdated.getPools().get(0).getVms().get(1);
                ImplementationPlan.Vm backupE2eeVm = implPlanUpdated.getPools().get(0).getVms().get(3);

                assertEquals(backupE2eeVm.getPublicIp(), null);
                assertTrue(backupE2eeVm.getComponents().get(0).getPrivateIps().isEmpty());
                assertEquals(slaveDbVm.getPublicIp(), null);
                assertTrue(slaveDbVm.getComponents().get(0).getPrivateIps().isEmpty());

                slaveDbVm.setPublicIp("194.102.62.100");
                slaveDbVm.getComponents().get(0).setPrivateIps(Arrays.asList("100.100.94.100"));

                backupE2eeVm.setPublicIp("194.102.62.101");
                backupE2eeVm.getComponents().get(0).setPrivateIps(Arrays.asList("100.100.94.101"));

                return implPlanUpdated;            }
        });

        forkAttackRemediator.remediate(remPlan, implPlan);

        ImplementationPlan implPlan1 = implPlanService.retrieveImplPlan(implPlan.getId());
        assertEquals(implPlan1.getSlaId(), implPlan.getSlaId());

        List<ImplementationPlan.Vm> vmList = implPlan1.getPools().get(0).getVms();

        assertEquals(vmList.get(0).getPublicIp(), "194.102.62.248");
        assertEquals(vmList.get(0).getVmSeqNum(), 0);
        assertEquals(vmList.get(0).getComponents().get(0).getId(), "DBB_main_db");

        assertEquals(vmList.get(1).getPublicIp(), "194.102.62.100");
        assertEquals(vmList.get(1).getVmSeqNum(), 1);
        assertEquals(vmList.get(1).getComponents().get(0).getId(), "DBB_backup_db");

        assertEquals(vmList.get(2).getPublicIp(), "194.102.62.196");
        assertEquals(vmList.get(2).getVmSeqNum(), 2);
        assertEquals(vmList.get(2).getComponents().get(0).getId(), "DBB_main_server");

        assertEquals(vmList.get(3).getPublicIp(), "194.102.62.101");
        assertEquals(vmList.get(3).getVmSeqNum(), 3);
        assertEquals(vmList.get(3).getComponents().get(0).getId(), "DBB_backup_server");

        assertEquals(vmList.get(4).getPublicIp(), "194.102.62.252");
        assertEquals(vmList.get(4).getVmSeqNum(), 4);
        assertEquals(vmList.get(4).getComponents().get(0).getId(), "DBB_auditor");
        assertEquals(vmList.get(4).getComponents().get(1).getId(), "DBB_monitoring_adapter");
    }
}