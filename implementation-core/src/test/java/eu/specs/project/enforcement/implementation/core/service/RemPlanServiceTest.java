package eu.specs.project.enforcement.implementation.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.implementation.core.TestParent;
import eu.specs.project.enforcement.implementation.core.mock.EventArchiverMock;
import eu.specs.project.enforcement.implementation.core.repository.ImplPlanRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:implementation-core-context-test.xml")
public class RemPlanServiceTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(RemPlanServiceTest.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RemPlanService remPlanService;

    @Autowired
    private ImplPlanRepository implPlanRepository;

    @Autowired
    private EventArchiverMock eventArchiverMock;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(
            wireMockConfig()
                    .port(Integer.parseInt(System.getProperty("wiremock.port")))
                    .extensions(new EventArchiverMock.EventArchiverMockTransformer()));

    @Before
    public void setUp() throws Exception {
        eventArchiverMock.init();
    }

    @Test
    public void testService() throws Exception {
        ImplementationPlan implPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/impl-plan-rem.json"), ImplementationPlan.class);

        implPlanRepository.save(implPlan);

        RemPlan remPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/rem-plan.json"), RemPlan.class);

        String remPlanId = remPlanService.implementRemPlan(remPlan);

        Date startTime = new Date();
        while (remPlan.getResult() == null) {
            Thread.sleep(500);
            if (new Date().getTime() - startTime.getTime() > 10000) {
                fail("Timeout waiting for the remediation plan to finish.");
            }
        }

        assertEquals(remPlan.getId(), remPlanId);
        assertEquals(remPlan.getResult(), RemPlan.Result.OBSERVE);
    }
}