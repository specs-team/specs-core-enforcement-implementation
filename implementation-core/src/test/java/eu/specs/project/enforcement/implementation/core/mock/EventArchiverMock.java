package eu.specs.project.enforcement.implementation.core.mock;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import eu.specs.datamodel.enforcement.MonitoringEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class EventArchiverMock {

    @Autowired
    private ObjectMapper objectMapper;

    public void init() throws IOException {

        List<MonitoringEvent> monitoringEvents = objectMapper.readValue(
                this.getClass().getResourceAsStream("/monitoring-events.json"),
                new TypeReference<List<MonitoringEvent>>() {
                });

        stubFor(get(urlPathEqualTo("/monitoring/events"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(monitoringEvents))
                        .withTransformers("EventArchiverMockTransformer")));
    }

    public static class EventArchiverMockTransformer extends ResponseTransformer {

        @Override
        public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource files) {
            String updatedEvent = responseDefinition.getBody()
                    .replaceAll("\"timestamp\":\\d+", "\"timestamp\":" + new Date().getTime());
            return ResponseDefinitionBuilder
                    .like(responseDefinition).but()
                    .withBody(updatedEvent)
                    .build();
        }

        @Override
        public String name() {
            return "EventArchiverMockTransformer";
        }

        @Override
        public boolean applyGlobally() {
            return false;
        }
    }
}
