package eu.specs.project.enforcement.implementation.core.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.implementation.core.TestParent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:implementation-core-context-test.xml")
public class RemPlanRepositoryTest extends TestParent {

    @Autowired
    private RemPlanRepository remPlanRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void test() throws Exception {
        RemPlan remPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/rem-plan.json"), RemPlan.class);

        remPlanRepository.save(remPlan);

        RemPlan remPlan1 = remPlanRepository.findById(remPlan.getId());
        assertEquals(remPlan1.getId(), remPlan.getId());

        remPlanRepository.delete(remPlan1);
        assertNull(remPlanRepository.findById(remPlan1.getId()));
    }
}