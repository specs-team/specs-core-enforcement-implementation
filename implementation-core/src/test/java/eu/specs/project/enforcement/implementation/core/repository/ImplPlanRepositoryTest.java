package eu.specs.project.enforcement.implementation.core.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.implementation.core.TestParent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:implementation-core-context-test.xml")
public class ImplPlanRepositoryTest extends TestParent {

    @Autowired
    private ImplPlanRepository implPlanRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void test() throws Exception {
        ImplementationPlan implPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/impl-plan.json"), ImplementationPlan.class);

        implPlanRepository.save(implPlan);

        ImplementationPlan implPlan1 = implPlanRepository.findById(implPlan.getId());
        assertEquals(implPlan1.getId(), implPlan.getId());

        List<ImplementationPlan> implPlans = implPlanRepository.getAll(implPlan.getSlaId());
        assertEquals(implPlans.size(), 1);
        assertEquals(implPlans.get(0).getId(), implPlan.getId());

        implPlanRepository.delete(implPlan);
        assertNull(implPlanRepository.findById(implPlan.getId()));
    }
}